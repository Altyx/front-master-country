import * as React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Country from './components/country';
import styled from "styled-components";
import Home from './components/Home';

const Container = styled.div`
    display: flex;
    max-width: 80%;
    width: 100%;
    margin-left: auto;
    margin-right: auto;
    flex-direction: column;
`;

const Header = styled.header`
background: hsl(209, 23%, 22%);
height: 50px;
color:white;
`;

const H1 = styled.h1`
  margin: 0;
  padding: 0 0 0 10%;
`;

function App() {
  return (
    <Router>
      <Header>
        <H1>Where in the world?</H1>
    </Header>
    <Container>
      <Switch>
        <Route exact path='/'>
          <Home />
        </Route>
        <Route path='/:id'>
          <Country />
        </Route>
      </Switch>
      </Container>
    </Router>
  );
}

export default App;
