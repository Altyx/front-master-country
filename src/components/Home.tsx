import * as React from 'react';
import {Link} from 'react-router-dom';
import styled from 'styled-components';


const Countries = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 75px;
`;

const Img = styled.img`
  object-fit: cover;
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
`;
const Links = styled(Link)`
  max-width: 100%;
  width: 100%;
  height: 150px;

`;

const Country = styled.div`
  background-color: hsl(209, 23%, 22%);
  height: 350px;
  display: flex;
  flex-direction: column;
  flex: 1 0 21%;
  gap: 10px;
  color: white;
  border-radius: 5px;;
`;

const Search = styled.div`
  margin: 30px 0;
  display: flex;
  justify-content: space-between;
  input {
    color: white;
    background: hsl(209,23%,22%);
    border-radius: 4px;
    border: none;
    outline: none;
    padding: 20px;
  }
  select {
    outline: none;
    background: hsl(209,23%,22%);
    color: white;
    padding: 13px;
    border: none;
    border-radius: 5px;
    padding-right: 34px;
  }
`;

type countryType = {
  [key: string]: string
};

const Home: React.FC = () => {
  const [countries, setCountries] = React.useState<countryType[]>([{}]);
  const [search, setSearch] = React.useState<string>('');
  const [region, setRegion] = React.useState<string>('all');

  React.useEffect(() => {
    fetch('https://restcountries.com/v2/all')
      .then((res) => res.json())
      .then((result) => setCountries(result))
      .catch((error) => console.log(error));
  }, []);

  const handleInput: (event: React.ChangeEvent<HTMLInputElement>) => void = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setSearch(event.target.value);
  };

  const handleRegion = (event: React.ChangeEvent<HTMLSelectElement>): void => {
    setRegion(event.target.value);
  };

  const filter: () => countryType[] = (): countryType[] => {
    if (search && region !== 'all') {
      return countries.filter((country) => country.name.toLowerCase().indexOf(search.toLowerCase()) !== -1 && country.region?.toLowerCase().indexOf(region) !== -1);
    }
    if (search) {
      return countries.filter((country:countryType) => country.name.toLowerCase().indexOf(search.toLowerCase()) !== -1);
    }
    if (region !== 'all') {
      return countries.filter((country:countryType) => country.region?.toLowerCase().indexOf(region.toLowerCase()) !== -1);
    }
    return countries;
  };

  return (
    <div>
      <Search>
        <input type="text" value={search} onChange={(event) => handleInput(event)} placeholder="Search for a country"/>
        <select name="region" id="region" onChange={handleRegion} placeholder="filter by region">
          <option value={"all"}>Filer by Region</option>
          <option value="africa">Africa</option>
          <option value="america">America</option>
          <option value="asia">Asia</option>
          <option value="europe">Europe</option>
          <option value="oceania">Oceania</option>
        </select>
      </Search>
      <Countries className="countries">
        {filter().map((country) => <Country key={country.alpha3Code}>
          <Links to={country.alpha3Code}>
            <Img src={country.flag} height='100%' width='100%'/>
          </Links>
          <div>{country.name}</div>
          <div>Population: {country.population}</div>
          <div>Region: {country.region}</div>
          <div>Capital: {country.capital}</div>
        </Country>)}
      </Countries>
    </div>
  )
}

export default Home;
