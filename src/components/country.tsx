import React, { useState, useEffect} from 'react';
import { useParams, Link } from 'react-router-dom';
import styled from "styled-components";

const Button = styled(Link)`
  height: 30px;
  background: hsl(209,23%,22%);
  width: 100px;
  color: white;
  text-decoration: none;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 50px  0;
  border-radius: 5px;
  box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;
`;

const Border = styled(Button)`
  margin: 0;
`;

const Borders = styled.div`
  display: flex;
  gap: 20px;
  flex-wrap: wrap;
  margin-top: 30px;
`;
const Container = styled.div`
  display: flex;
  gap: 80px;
  color: white;
`;
const Flag = styled.div`
  width: 100%;
`;

const Title = styled.div`
  font-size: 26px;
  font-weight: 600;
  margin: 30px 0;
`;

const Detail = styled.div`
  display: flex;
  gap: 130px;
`;

const Data = styled.div`
  margin: 10px 0;
  span {
    font-weight: 600;
  }
`;

interface countryId {
  id: string
};

type countryType = {
  flag: string,
  name: string,
  nativeName: string,
  region: string,
  subregion: string,
  capital: string,
  topLevelDomain: string,
  population: number,
  borders?: [string],
  currencies?: [{
    name:string,
  }],
  languages?: [{
    name: string,
  }],
};

const Country: React.FC = () => {
  const { id } = useParams<countryId>();
  const [country, set] = useState<countryType>({} as countryType);

  useEffect(() => {
    fetch(`https://restcountries.com/v2/alpha/${id}`)
      .then((res) => res.json())
      .then((result) => set(result))
      .catch((error) => console.log(error));
  }, [id]);

  return (
    <>
      <Button to={'/'}>Back</Button>
      {JSON.stringify(country)}
      <Container>
        <Flag><img src={country.flag} alt={""} width={'100%'} /></Flag>
        <Flag>
          <Title>{country.name}</Title>
          <Detail>
            <div>
              <Data><span>Native Name:</span> {country.nativeName}</Data>
              <Data><span>Population:</span> {country.population}</Data>
              <Data><span>Region:</span> {country.region}</Data>
              <Data><span>Sub Region:</span> {country.subregion}</Data>
              <Data><span>Capital:</span> {country.capital}</Data>
            </div>
            <div>
              <Data><span>Top Level Domaine:</span> {country.topLevelDomain}</Data>
              <Data><span>Currencies:</span> {country.currencies?.map((cur) => cur?.name)}</Data>
              <Data><span>Languages:</span> {country.languages?.map((lang) => lang?.name)}</Data>
            </div>
          </Detail>
          {country.borders && <Borders>Border Countries: {country.borders?.map((border) => <Border to={border}>{border}</Border>)}</Borders>}
        </Flag>
      </Container>
    </>
  );
};

export default Country;
